"""
Usage:
	python3 create-macOS-app.py py2app
"""

from setuptools import setup

APP = ['stri.py']
DATA_FILES = [('', ['assets'])]
OPTIONS = {
	'strip': True,
	'iconfile': 'icon.icns',
	'packages': 'pysam'
}

setup(
	name='STRipy',
	app=APP,
	data_files=DATA_FILES,
	options={'py2app': OPTIONS},
	setup_requires=['py2app'],
)
